package pl.bpawlowski.employee_management.core.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bpawlowski.employee_management.persistence.model.spec.Employee;
import pl.bpawlowski.employee_management.persistence.repositories.EmployeeRepository;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 *
 * Klasa bedaca delegatem do repozytorium pracownikow, caly modul bylby potrzebny gdybysmy chcieli implementowac zapytania przy pomocy criteria-api
 * lub potrzebna bylaby aktywna transakcja, ktorej nie ma w kontrolerach w module WEB.
 */
@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public <T extends Employee> T save(T employee) {
        return employeeRepository.save(employee);
    }

    public Iterable<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public void delete(Long id) {
        employeeRepository.delete(id);
    }

    public void delete(Employee employee) {
        employeeRepository.delete(employee);
    }

    public Iterable<Employee> findAll(Iterable<Long> ids) {
        return employeeRepository.findAll(ids);
    }

    public <T extends Employee> Iterable<T> save(Iterable<T> employeeCollection) {
        return employeeRepository.save(employeeCollection);
    }

    public void deleteAll() {
        employeeRepository.deleteAll();
    }

    public void delete(Iterable<? extends Employee> employees) {
        employeeRepository.delete(employees);
    }

    public Employee findOne(Long id) {
        return employeeRepository.findOne(id);
    }

    public long count() {
        return employeeRepository.count();
    }

    public boolean exists(Long id) {
        return employeeRepository.exists(id);
    }
}
