package pl.bpawlowski.employee_management.persistence.model.spec;

import pl.bpawlowski.employee_management.persistence.model.AbstractEntity;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 *
 * Encja Employee - bez adnotacji z pakietu javax.persistence - nie bylo takiej koniecznosci
 */
public class Employee extends AbstractEntity{

    private String name;

    private int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
