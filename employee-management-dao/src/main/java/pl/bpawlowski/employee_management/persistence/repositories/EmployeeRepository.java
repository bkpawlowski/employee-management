package pl.bpawlowski.employee_management.persistence.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pl.bpawlowski.employee_management.persistence.model.spec.Employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 *
 *
 * Klasa odpowiadajaca za przechowywanie pracownikow w pamieci - implementuje interfejs repozytorium ze Spring-data-jpa
 */
@Service
public class EmployeeRepository implements CrudRepository<Employee, Long> {

    private HashMap<Long, Employee> employeesMap;
    private long employeeCounter;

    public EmployeeRepository() {
        employeesMap = new HashMap<>();
    }

    @Override
    public <T extends Employee> T save(T employee) {
        /* tak jak w spring-data-jpa obsluguje utworzenie nowej encji, jak i aktualizacje */
        Long id = employee.getId();
        if(id == null){
            id = employeeCounter++;
            employee.setId(id);
        }else{
            employeesMap.remove(id);
        }

        employeesMap.put(id, employee);

        return employee;
    }

    @Override
    public <T extends Employee> Iterable<T> save(Iterable<T> employeeCollection) {
        List<T> employees = new ArrayList<>();
        for (T se : employeeCollection) {
            employees.add(save(se));
        }
        return employees;
    }

    @Override
    public Employee findOne(Long id) {
        return employeesMap.get(id);
    }

    @Override
    public boolean exists(Long id) {
        return employeesMap.containsKey(id);
    }

    @Override
    public Iterable<Employee> findAll() {
        return employeesMap.values();
    }

    @Override
    public Iterable<Employee> findAll(Iterable<Long> ids) {
        final List<Employee> result = new ArrayList<>();

        for (Long id : ids) {
            final Employee employee = employeesMap.get(id);
            if (employee != null) {
                result.add(employee);
            }
        }

        return result;
    }

    @Override
    public long count() {
        return employeesMap.size();
    }

    @Override
    public void delete(Long id) {
        employeesMap.remove(id);
    }

    @Override
    public void delete(Employee employee) {
        if(employee != null) {
            employeesMap.remove(employee.getId());
        }
    }

    @Override
    public void delete(Iterable<? extends Employee> employees) {
        for (Employee employee : employees) {
            delete(employee);
        }
    }

    @Override
    public void deleteAll() {
        employeesMap = new HashMap<>();
    }
}
