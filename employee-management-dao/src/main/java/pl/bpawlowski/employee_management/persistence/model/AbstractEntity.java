package pl.bpawlowski.employee_management.persistence.model;

/**
 * Author: Bartek Pawlowski
 * 2014-12-16.
 *
 * Abstrakcyjna encja - bez adnoctacji z pakietu javax.persistence
 */
public abstract class AbstractEntity{

    protected Long id;

    public Long getId(){
        return id;
    }

    public void setId(Long id){
        this.id = id;
    }
}
