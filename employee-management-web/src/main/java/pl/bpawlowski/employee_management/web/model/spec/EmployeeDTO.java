package pl.bpawlowski.employee_management.web.model.spec;

import pl.bpawlowski.employee_management.web.model.AbstractDTO;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 *
 * Klasa DTO serializowana przez kontrolery w module WEB - nie zwraca sie encji ze wzgledu na sesje i stan encji ktora zarzadza entity manager
 * DTO jest najczesciej klonem encji lub jej podzbiorem(wycinkiem) na potrzeby API/GUI
 */
@XmlRootElement
public class EmployeeDTO extends AbstractDTO {
    private String name;

    private int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
