package pl.bpawlowski.employee_management.web.model;

/**
 * Author: Bartek Pawlowski
 * 2015-02-25.
 *
 * Abstrakcyjne DTO
 */
public abstract class AbstractDTO {
    protected Long id;

    protected AbstractDTO(){

    }

    protected AbstractDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
