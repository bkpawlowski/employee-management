package pl.bpawlowski.employee_management.web.marshalling;

import org.springframework.http.HttpHeaders;
import pl.bpawlowski.employee_management.persistence.model.spec.Employee;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Author: Bartek Pawlowski
 * 2015-01-04.
 */
public class Employee2XMLHttpMessageConverter extends MyAbstractObject2XMLHttpMessageConverter {
    @Override
    protected Object readFromSource(Class clazz, HttpHeaders headers, Source source) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void writeToResult(Object o, HttpHeaders headers, Result result) throws IOException {
        Marshaller marshaller = createMarshaller2();
        try {
            marshaller.marshal(o, result);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * copy from AbstractJaxb2HttpMessageConverter
     */
    protected final Marshaller createMarshaller2() {
        //workaround, nie nie wiem jak to zadziala z zagniezdzonymi customowymi obiektami - pewnie nie zadziala ;)
        List<Class<?>> classes = new LinkedList<>();
        classes.add(Employee.class);

        return createMarshaller(classes);
    }

    @Override
    protected boolean supports(Class clazz) {
        if(clazz == Employee.class){
            return true;
        }
        return false;
    }
}
