package pl.bpawlowski.employee_management.web.transformers;

import org.springframework.data.repository.CrudRepository;
import pl.bpawlowski.employee_management.persistence.model.AbstractEntity;
import pl.bpawlowski.employee_management.web.model.AbstractDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Bartek Pawlowski
 * 2014-12-16.
 *
 * Generyczny dwukierynkuowy transformer DTO<=>Encja
 */
public abstract class GenericTransformer<EntityClass extends AbstractEntity, DTOClass extends AbstractDTO> {

    protected CrudRepository<EntityClass, Long> entityRepository;

    protected GenericTransformer(CrudRepository<EntityClass, Long> entityRepository) {
        this.entityRepository = entityRepository;
    }

    public abstract void updateEntity(DTOClass in, EntityClass out);
//    protected abstract void updateEntity(DTOClass in, EntityClass out);
    protected abstract EntityClass createEntity();
    protected abstract DTOClass createDTO(EntityClass in);

    public EntityClass toEntity(Long id){
        if(id != null) {
            return entityRepository.findOne(id);
        }else{
            return null;
        }
    }

    public EntityClass toEntity(DTOClass in){
        return toEntity(false, in);
    }

    public EntityClass toEntity(boolean allowUpdate, DTOClass in){
        if(in == null){
            return null;
        }

        final EntityClass out;

        if(in.getId() != null){
            out = toEntity(in.getId());
        }else{
            out = createEntity();
            updateEntity(in, out);
        }

        if(allowUpdate) {
            updateEntity(in, out);
        }

        return out;
    }

    public DTOClass toDTO(EntityClass entity){
        return createDTO(entity);
    }

    public List<Long> toLongList(Iterable<? extends EntityClass> in){
        List<Long> out = new ArrayList<Long>();

        if(in != null) {
            for (EntityClass inEntry : in) {
                out.add(inEntry.getId());
            }
        }

        return out;
    }

    public List<DTOClass> toDTOList(Iterable<EntityClass> in){
        final List<DTOClass> out = new ArrayList<>();

        if(in != null) {
            for (EntityClass inEntry : in) {
                out.add(toDTO(inEntry));
            }
        }

        return out;
    }

    public List<EntityClass> toEntityList(List<DTOClass> in){
        final List<EntityClass> out = new ArrayList<>();

        if(in != null) {
            for (DTOClass inEntry : in) {
                out.add(toEntity(inEntry));
            }
        }

        return out;
    }

    public List<Long> toIdList(List<EntityClass> in){
        final List<Long> out = new ArrayList<>();

        if(in != null){
            for(EntityClass inEntry : in){
                out.add(inEntry.getId());
            }
        }
        return out;
    }

    public List<EntityClass> toEntityListByIds(List<Long> in){
        final List<EntityClass> out = new ArrayList<EntityClass>();
        if(in != null) {
            for (Long inEntry : in) {
                out.add(toEntity(inEntry));
            }
        }
        return out;
    }

    public Long getId(AbstractEntity in){
        if(in != null){
            return in.getId();
        }

        return null;
    }
}


