package pl.bpawlowski.employee_management.web.marshalling;

import org.springframework.http.HttpHeaders;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Author: Bartek Pawlowski
 * 2014-12-30.
 *
 * Prosty konwerter kolekcji do wiadomosci http w formacie xml
 */
public class Collection2XMLHTTPMessageConverter extends MyAbstractObject2XMLHttpMessageConverter {
    @Override
    protected Object readFromSource(Class clazz, HttpHeaders headers, Source source) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void writeToResult(Object o, HttpHeaders headers, Result result) throws IOException {
        Collection<?> collection = (Collection<?>) o;
        JAXBCollectionWrapper<?> collectionWrapper = new JAXBCollectionWrapper<>(collection);
        Marshaller marshaller = createMarshaller2(collection);
        try {
            marshaller.marshal(collectionWrapper, result);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * copy from AbstractJaxb2HttpMessageConverter
     */
    protected final Marshaller createMarshaller2(Collection collection) {
        //workaround, nie nie wiem jak to zadziala z zagniezdzonymi customowymi obiektami - pewnie nie zadziala ;)
        Set<Class<?>> classesSet = new HashSet<>();
        Iterator collectionIter = collection.iterator();
        while (collectionIter.hasNext()){
            Object collectionObject = collectionIter.next();
            Class<?> clazz = collectionObject.getClass();
            if(!clazz.isInterface()) {
                classesSet.add(clazz);
            }
        }
        classesSet.add(JAXBCollectionWrapper.class);

        return createMarshaller(classesSet);
    }

    @Override
    protected boolean supports(Class clazz) {
        try {
            Object clazzInstance = clazz.newInstance();
            if (clazzInstance instanceof Collection) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
