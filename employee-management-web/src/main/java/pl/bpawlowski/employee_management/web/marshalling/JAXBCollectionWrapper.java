package pl.bpawlowski.employee_management.web.marshalling;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Author: Bartek Pawlowski
 * 2014-12-29.
 */
@XmlRootElement(name = "XMLcollection")
@XmlAccessorType(XmlAccessType.FIELD)
public class JAXBCollectionWrapper<Entity extends Object> {

    @XmlAnyElement
    private Collection<Entity> collection;

    public JAXBCollectionWrapper() {
        collection = new ArrayList<>();
    }

    public JAXBCollectionWrapper(Collection<Entity> collection) {
        this.collection = collection;
    }

    public Collection<Entity> getCollection() {
        return collection;
    }

    public void setCollection(Collection<Entity> collection) {
        this.collection = collection;
    }
}
