package pl.bpawlowski.employee_management.web.marshalling;

import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.xml.AbstractJaxb2HttpMessageConverter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.util.Collection;

/**
 * Author: Bartek Pawlowski
 * 2015-01-04.
 */
public abstract class MyAbstractObject2XMLHttpMessageConverter<MarshalledObjectClass> extends AbstractJaxb2HttpMessageConverter {
    protected final Marshaller createMarshaller(Collection<Class> classesCollection) {
        try {
            Class[] classes = classesCollection.toArray(new Class[]{});

            //ponoc kosztowna operacja, na razie zostawiam + plus nigdy go nie zachowuje :/
            JAXBContext jaxbContext = JAXBContext.newInstance(classes);
            Marshaller marshaller = jaxbContext.createMarshaller();
            return marshaller;
        } catch (JAXBException ex) {
            StringBuilder stringifiedClasses = new StringBuilder();
            for (Class clazz : classesCollection) {
                stringifiedClasses.append(clazz + ", ");
            }
            throw new HttpMessageConversionException(
                    "Could not create Marshaller for classes [" + stringifiedClasses.toString() + "]: " + ex.getMessage(), ex);
        }
    }
}
