package pl.bpawlowski.employee_management.web.transformers.spec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bpawlowski.employee_management.persistence.model.spec.Employee;
import pl.bpawlowski.employee_management.persistence.repositories.EmployeeRepository;
import pl.bpawlowski.employee_management.web.model.spec.EmployeeDTO;
import pl.bpawlowski.employee_management.web.transformers.GenericTransformer;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 *
 * Detykowany dwukierunkowy transformer DTO<=>Encja na potrzeby Rest-API
 */
@Service
public class EmployeeTransformer extends GenericTransformer<Employee, EmployeeDTO> {

    @Autowired
    protected EmployeeTransformer(EmployeeRepository entityRepository) {
        super(entityRepository);
    }

    @Override
    public void updateEntity(EmployeeDTO in, Employee out) {
        out.setId(in.getId());
        out.setName(in.getName());
        out.setSalary(in.getSalary());

        entityRepository.save(out);
    }

    @Override
    protected Employee createEntity() {
        return new Employee();
    }

    @Override
    protected EmployeeDTO createDTO(Employee in) {
        if(in == null){
            return null;
        }
        final EmployeeDTO out = new EmployeeDTO();
        out.setId(in.getId());
        out.setName(in.getName());
        out.setSalary(in.getSalary());

        return out;
    }
}
