package pl.bpawlowski.employee_management.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.bpawlowski.employee_management.core.services.EmployeeService;
import pl.bpawlowski.employee_management.persistence.model.spec.Employee;
import pl.bpawlowski.employee_management.web.model.spec.EmployeeDTO;
import pl.bpawlowski.employee_management.web.transformers.spec.EmployeeTransformer;

import java.util.List;

/**
 * Author: Bartek Pawlowski
 * 2015-06-11.
 */
@RestController()
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeTransformer employeeTransformer;

    @Autowired
    private EmployeeService employeeService;


    @RequestMapping(value = "/new", method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody ResponseEntity<Long> saveEmployee(@RequestBody EmployeeDTO employeeDTO){
        final Employee employee = employeeTransformer.toEntity(employeeDTO);

        employeeService.save(employee);

        return new ResponseEntity<>(employee.getId(), HttpStatus.OK);
    }

    @RequestMapping(value = "/update/{id}/{name}/{salary}", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
     public @ResponseBody ResponseEntity<String> updateEmployee(@PathVariable Long id, @PathVariable String name, @PathVariable int salary){
        final Employee employee = employeeTransformer.toEntity(id);

        HttpStatus httpStatus;
        if(employee != null){
            employee.setName(name);
            employee.setSalary(salary);
            employeeService.save(employee);
            httpStatus = HttpStatus.OK;
        }else{
            httpStatus = HttpStatus.NOT_ACCEPTABLE;
        }

        return new ResponseEntity<>("", httpStatus);
    }

    @RequestMapping(value = "/update/{id}/{salary}", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    public @ResponseBody ResponseEntity<String> updateEmployeeSalary(@PathVariable Long id, @PathVariable int salary){
        final Employee employee = employeeTransformer.toEntity(id);

        HttpStatus httpStatus;
        if(employee != null){
            employee.setSalary(salary);
            employeeService.save(employee);
            httpStatus = HttpStatus.OK;
        }else{
            httpStatus = HttpStatus.NOT_ACCEPTABLE;
        }

        return new ResponseEntity<>("", httpStatus);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = {"application/json", "application/xml"})
    public @ResponseBody ResponseEntity<String> deleteEmployeeById(@PathVariable Long id){
        employeeService.delete(id);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    public @ResponseBody ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable Long id){
        return new ResponseEntity<>(employeeTransformer.toDTO(employeeService.findOne(id)), HttpStatus.OK);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = {"application/json", "application/xml"})
    public @ResponseBody ResponseEntity<List<EmployeeDTO>> getEmployees(){
        return new ResponseEntity<>(employeeTransformer.toDTOList(employeeService.findAll()), HttpStatus.OK);
    }
}
